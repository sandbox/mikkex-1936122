<?php
/**
 * @file
 * Alter the autocomplete of taxonomys
 */

/**
 * The admin form
 */
function autocomplete_limit_admin() {
  $form = array();
  $form['autocomplete_limit_taxonomy'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of taxonomy terms to display'),
    '#default_value' => variable_get('autocomplete_limit_taxonomy',10),
    '#size' => 5,
    '#maxlength' => 5,
    '#description' => t("Number of taxonomy terms to display in a autocomplete. Numbers only."),
    '#required' => TRUE,
  );
  $form['autocomplete_limit_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of users to display'),
    '#default_value' => variable_get('autocomplete_limit_user',10),
    '#size' => 5,
    '#maxlength' => 5,
    '#description' => t("Number of users to display in a autocomplete. Numbers only."),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}


/**
 * Implements hook_form_validate().
 */
function autocomplete_limit_admin_validate($form, $form_state){
  if (!is_numeric($form_state['values']['autocomplete_limit_taxonomy'])) {
    form_set_error('autocomplete_limit_taxonomy', t('You must enter a number.'));
  }
  if (!is_numeric($form_state['values']['autocomplete_limit_user'])) {
    form_set_error('autocomplete_limit_user', t('You must enter a number.'));
  }
}


/**
 * Implements hook_menu().
 */
function autocomplete_limit_menu() {
  $items = array();
  $items['admin/config/autocomplete_limit'] = array(
    'title' => 'Autocomplete limit',
    'description' => 'Settings for number of autocomplete posts to show',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('autocomplete_limit_admin'),
    'access arguments' => array('administer autocomplete limit'),
    'type' => MENU_NORMAL_ITEM,
   );

  return $items;
}

/**
 * Implements hook_permission
 */
function hook_permission() {
  return array(
    'administer autocomplete limit' => array(
      'title' => t('Administer Autocpmplete limit'), 
      'description' => t('Administrater settings for Autocomplete limit.'),
    ),
  );
}

/**
 * Implements hook_menu_alter().
 */ 
function autocomplete_limit_menu_alter(&$items) {
  if (isset($items['taxonomy/autocomplete'])) {
    $items['taxonomy/autocomplete']['page callback'] = 'autocomplete_limit_taxonomy_autocomplete';
  }
  if (isset($items['user/autocomplete'])) {
    $items['user/autocomplete']['page callback'] = 'autocomplete_limit_user_autocomplete';
  }
}

/**
 * @see: taxonomy_autocomplete
 */
function autocomplete_limit_taxonomy_autocomplete($field_name, $tags_typed = '') {
  // If the request has a '/' in the search text, then the menu system will have
  // split it into multiple arguments, recover the intended $tags_typed.
  $args = func_get_args();
  // Shift off the $field_name argument.
  array_shift($args);
  $tags_typed = implode('/', $args);

  // Make sure the field exists and is a taxonomy field.
  if (!($field = field_info_field($field_name)) || $field['type'] !== 'taxonomy_term_reference') {
    // Error string. The JavaScript handler will realize this is not JSON and
    // will display it as debugging information.
    print t('Taxonomy field @field_name not found.', array('@field_name' => $field_name));
    exit;
  }

  // The user enters a comma-separated list of tags. We only autocomplete the last tag.
  $tags_typed = drupal_explode_tags($tags_typed);
  $tag_last = drupal_strtolower(array_pop($tags_typed));

  $matches = array();
  if ($tag_last != '') {

    // Part of the criteria for the query come from the field's own settings.
    $vids = array();
    $vocabularies = taxonomy_vocabulary_get_names();
    foreach ($field['settings']['allowed_values'] as $tree) {
      $vids[] = $vocabularies[$tree['vocabulary']]->vid;
    }

    $query = db_select('taxonomy_term_data', 't');
    $query->addTag('translatable');
    $query->addTag('term_access');

    // Do not select already entered terms.
    if (!empty($tags_typed)) {
      $query->condition('t.name', $tags_typed, 'NOT IN');
    }

    // Added for getting custom number of results
    $number_of_results = variable_get('autocomplete_limit_taxonomy',10);

    // Select rows that match by term name.
    $tags_return = $query
      ->fields('t', array('tid', 'name'))
      ->condition('t.vid', $vids)
      ->condition('t.name', '%' . db_like($tag_last) . '%', 'LIKE')
      ->range(0, $number_of_results)
      ->execute()
      ->fetchAllKeyed();

    $prefix = count($tags_typed) ? drupal_implode_tags($tags_typed) . ', ' : '';

    $term_matches = array();
    foreach ($tags_return as $tid => $name) {
      $n = $name;
      // Term names containing commas or quotes must be wrapped in quotes.
      if (strpos($name, ',') !== FALSE || strpos($name, '"') !== FALSE) {
        $n = '"' . str_replace('"', '""', $name) . '"';
      }
      $term_matches[$prefix . $n] = check_plain($name);
    }
  }

  drupal_json_output($term_matches);
}


/**
 * @see: user_complete
 */
function autocomplete_limit_user_autocomplete($string = '') {
  $matches = array();
  if ($string) {
    
     // Added for getting custom number of results
    $number_of_results = variable_get('autocomplete_limit_user',10);
    $result = db_select('users')->fields('users', array('name'))->condition('name', db_like($string) . '%', 'LIKE')->range(0, $number_of_results)->execute();
    foreach ($result as $user) {
      $matches[$user->name] = check_plain($user->name);
    }
  }

  drupal_json_output($matches);
}


function autocomplete_limit_help($path, $arg) {
  switch ($path) {
    // Main module help for the block module
    case 'admin/help#autocomplete_limit':
      return '<p>' . t('Autocomplete limit alters the number of autocomplete 
        results to get. Settings are stored in variables, which could be set 
        on the configuration page or some other way.') . '</p>';
  }
}
